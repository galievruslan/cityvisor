require 'test_helper'

class OfficePhotosControllerTest < ActionController::TestCase
  setup do
    @office_photo = office_photos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:office_photos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create office_photo" do
    assert_difference('OfficePhoto.count') do
      post :create, office_photo: { office_id: @office_photo.office_id }
    end

    assert_redirected_to office_photo_path(assigns(:office_photo))
  end

  test "should show office_photo" do
    get :show, id: @office_photo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @office_photo
    assert_response :success
  end

  test "should update office_photo" do
    patch :update, id: @office_photo, office_photo: { office_id: @office_photo.office_id }
    assert_redirected_to office_photo_path(assigns(:office_photo))
  end

  test "should destroy office_photo" do
    assert_difference('OfficePhoto.count', -1) do
      delete :destroy, id: @office_photo
    end

    assert_redirected_to office_photos_path
  end
end
