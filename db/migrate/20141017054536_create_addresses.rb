class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :country, index: true
      t.references :state, index: true
      t.references :city, index: true
      t.references :street, index: true
      t.references :house, index: true
      t.references :office, index: true

      t.timestamps
    end
  end
end
