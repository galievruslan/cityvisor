class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.integer :number
      t.string :corps
      t.decimal :latitude, :precision => 10, :scale => 6
      t.decimal :longitude, :precision => 10, :scale => 6
      t.references :street, index: true

      t.timestamps
    end
  end
end
