class CreateOfficePhotos < ActiveRecord::Migration
  def change
    create_table :office_photos do |t|
      t.references :office, index: true
      t.string :file
      t.timestamps
    end
  end
end
