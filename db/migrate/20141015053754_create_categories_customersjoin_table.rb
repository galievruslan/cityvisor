class CreateCategoriesCustomersjoinTable < ActiveRecord::Migration
  def change
    create_join_table :categories, :customers do |t|
      # t.index [:category_id, :customer_id]
      # t.index [:customer_id, :category_id]
    end
  end
end
