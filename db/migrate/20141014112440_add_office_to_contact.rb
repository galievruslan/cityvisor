class AddOfficeToContact < ActiveRecord::Migration
  def change
    add_reference :contacts, :office, index: true
  end
end
