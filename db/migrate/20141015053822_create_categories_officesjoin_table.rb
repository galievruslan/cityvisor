class CreateCategoriesOfficesjoinTable < ActiveRecord::Migration
  def change
    create_join_table :categories, :offices do |t|
      # t.index [:category_id, :office_id]
      # t.index [:office_id, :category_id]
    end
  end
end
