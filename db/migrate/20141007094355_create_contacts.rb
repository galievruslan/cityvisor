class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.actable
      t.timestamps
    end
  end
end
