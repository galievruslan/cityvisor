# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

role_admin = Role.create(name: 'admin')
role_customer = Role.create(name: 'customer')

user_admin = User.create(username: 'admin', password: '423200', password_confirmation: '423200', email: 'admin@test.com', language: 'ru')
user_admin.roles << role_admin
user_admin.save