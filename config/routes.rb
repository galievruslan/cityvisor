Rails.application.routes.draw do
  devise_for :users
  get 'private/uploads/:asset_type/:file', to: 'private_assets#show'

  root 'pages#index'
  resources :users do
    member do
      get :edit_password
      put :update_password
    end
  end
  resources :customers do
    resources :offices do
      resources :contacts, only: [:index]
      resources :phones
      resources :emails
      resources :sites
      resources :office_photos, path: 'photos'
      resources :addresses, except: [:index]
    end
  end
  resources :categories
  resources :countries do
    member do
      get :states
    end
  end
  resources :states do
    member do
      get :cities
    end
  end
  resources :cities do
    member do
      get :get_streets
    end
    resources :streets do
      member do
        get :get_houses
      end
      resources :houses
    end
    collection do
      get :states_in_country
    end
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
