class Street < ActiveRecord::Base
  validates :name, :city, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  belongs_to :city
  delegate :state, :to => :city, :allow_nil => true
  has_many :houses, :dependent => :destroy
end
