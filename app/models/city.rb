class City < ActiveRecord::Base
  validates :name, :state, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  belongs_to :state
  delegate :country, :to => :state, :allow_nil => true
  has_many :streets, :dependent => :destroy
end
