class State < ActiveRecord::Base
  validates :name, :country, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  belongs_to :country
  has_many :cities, :dependent => :destroy
end
