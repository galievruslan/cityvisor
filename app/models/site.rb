class Site < ActiveRecord::Base
  acts_as :contact, dependent: :delete
  validates :address, presence: true
  validates :address, url: {:message => I18n.t('errors.messages.is_not_valid_url')}
  validates :address, :uniqueness => { :case_sensitive => false }
end
