class Customer < ActiveRecord::Base
  validates :name, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  belongs_to :user
  has_and_belongs_to_many :categories
  has_many :offices, :dependent => :destroy
end
