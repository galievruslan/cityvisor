class Category < ActiveRecord::Base
  has_ancestry
  validates :name, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  has_and_belongs_to_many :customers
  has_and_belongs_to_many :offices
end
