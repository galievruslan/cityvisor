class Email < ActiveRecord::Base
  acts_as :contact, dependent: :delete
  validates :box, presence: true
  validates :box, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, message: I18n.t('errors.messages.invalid_email') }
  validates :box, :uniqueness => { :case_sensitive => false }
end
