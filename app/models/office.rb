class Office < ActiveRecord::Base
  validates :name, :customer, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  belongs_to :customer
  has_and_belongs_to_many :categories
  has_many :contacts, :dependent => :destroy
  has_many :office_photos, :dependent => :destroy
  has_one :address, :dependent => :destroy
end
