class Address < ActiveRecord::Base
  validates :country, :state, :city, :street, :house, :office, presence: true
  belongs_to :country
  belongs_to :state
  belongs_to :city
  belongs_to :street
  belongs_to :house
  belongs_to :office
end
