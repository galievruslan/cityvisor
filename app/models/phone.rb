class Phone < ActiveRecord::Base
  acts_as :contact, dependent: :delete
  PHONE_TYPES = {1 => 'base', 2 => 'mobile', 3 => 'fax'}
  validates :number, :phone_type, presence: true
  validates :phone_type, numericality: { only_integer: true }
  validates :number, format: { with: /\A\+\d{1}\(\d{3}\)\d{3}-\d{2}-\d{2}\z/ }
  validates_inclusion_of :phone_type, :in => PHONE_TYPES.keys, :message => I18n.t('errors.messages.invalid')
  validates :number, :uniqueness => { :case_sensitive => false }
end
