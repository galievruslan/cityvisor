class House < ActiveRecord::Base
  validates :number, :street, presence: true
  belongs_to :street
  delegate :city, :to => :street, :allow_nil => true
  validates :number, numericality: { only_integer: true, greater_than: 0 }
  validates :latitude, numericality: true, :allow_nil => true
  validates :longitude, numericality: true, :allow_nil => true
end
