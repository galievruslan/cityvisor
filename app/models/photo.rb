class Photo < ActiveRecord::Base
  self.abstract_class = true
  validates :file, presence: true
  mount_uploader :file, PhotoUploader
end
