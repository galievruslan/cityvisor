class OfficePhoto < Photo
  belongs_to :office
  validates :office, presence: true
end
