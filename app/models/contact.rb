class Contact < ActiveRecord::Base
  actable
  validates :office_id, presence: true
  CONTACT_TYPES = {'Phone' => I18n.t(:phone), 'Email' => I18n.t(:email), 'Site' => I18n.t(:site)}
  belongs_to :office
end
