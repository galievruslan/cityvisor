class Country < ActiveRecord::Base
  validates :name, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  has_many :states, :dependent => :destroy
  has_many :cities, :through => :states
end
