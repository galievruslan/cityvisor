class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user!
  before_filter :set_language_from_current_user

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  layout :layout

  def layout
    if devise_controller? && devise_mapping.name == :user
      "logon"
    else
      "application"
    end
  end

  def set_language_from_current_user
    if current_user and current_user.language == 'RU'
      I18n.default_locale = :ru
      I18n.locale = :ru 
    elsif current_user and current_user.language == 'EN'
      I18n.default_locale = :en
      I18n.locale = :en 
    else
      I18n.default_locale = :ru
      I18n.locale = :ru
    end   
  end

end
