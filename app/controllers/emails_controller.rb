class EmailsController < ApplicationController
  load_and_authorize_resource
  before_action :set_email, only: [:show, :edit, :update, :destroy]
  before_action :set_customer
  before_action :set_office

  # GET /emails
  # GET /emails.json
  def index
    @search = Email.where(office_id: @office.id).search(params[:q])
    @emails = @search.result.page(params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @emails }
    end
  end

  # GET /emails/1
  # GET /emails/1.json
  def show
  end

  # GET /emails/new
  def new
    @email = Email.new
  end

  # GET /emails/1/edit
  def edit
  end

  # POST /emails
  # POST /emails.json
  def create
    @email = Email.new(email_params)
    @office.contacts << @email
    respond_to do |format|
      if @email.save
        format.html { redirect_to customer_office_contacts_path(@customer, @office), notice: t(:email_created) }
        format.json { render :show, status: :created, location: @email }
      else
        format.html { render :new }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emails/1
  # PATCH/PUT /emails/1.json
  def update
    respond_to do |format|
      if @email.update(email_params)
        format.html { redirect_to customer_office_contacts_path(@customer, @office), notice: t(:email_updated) }
        format.json { render :show, status: :ok, location: @email }
      else
        format.html { render :edit }
        format.json { render json: @email.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emails/1
  # DELETE /emails/1.json
  def destroy
    @email.destroy
    respond_to do |format|
      format.html { redirect_to customer_office_contacts_path(@customer, @office), notice: t(:email_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_email
      @email = Email.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_office
      @office = @customer.offices.find(params[:office_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def email_params
      params.require(:email).permit(:box)
    end
end
