class OfficesController < ApplicationController
  load_and_authorize_resource
  before_action :set_customer
  before_action :set_office, only: [:show, :edit, :update, :destroy]

  # GET /offices
  # GET /offices.json
  def index
    @search = @customer.offices.search(params[:q])
    @offices = @search.result.page(params[:page])
    @photos_count = OfficePhoto.group(:office_id).count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @offices }
    end
  end

  # GET /offices/1
  # GET /offices/1.json
  def show
    @search = @office.contacts.search(params[:q])
    @contacts = @search.result.page(params[:page])
  end

  # GET /offices/new
  def new
    @office = Office.new
    @categories = Category.all
  end

  # GET /offices/1/edit
  def edit
    @categories = Category.all
  end

  # POST /offices
  # POST /offices.json
  def create
    @categories = Category.all
    @office = @customer.offices.new(office_params)

    respond_to do |format|
      if @office.save

        format.html { redirect_to customer_offices_path(@customer), notice: t(:office_created) }
        format.json { render :show, status: :created, location: @office }
      else
        format.html { render :new }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offices/1
  # PATCH/PUT /offices/1.json
  def update
    @categories = Category.all
    respond_to do |format|
      if @office.update(office_params)
        format.html { redirect_to customer_offices_path(@customer), notice: t(:office_updated) }
        format.json { render :show, status: :ok, location: @office }
      else
        format.html { render :edit }
        format.json { render json: @office.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offices/1
  # DELETE /offices/1.json
  def destroy
    @office.destroy
    respond_to do |format|
      format.html { redirect_to customer_offices_path(@customer), notice: t(:office_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_office
      @office = Office.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_params
      params.require(:office).permit(:name, :desc, :category_ids => [])
    end
end
