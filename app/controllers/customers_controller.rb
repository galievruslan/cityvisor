class CustomersController < ApplicationController
  load_and_authorize_resource
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers
  # GET /customers.json
  def index
    @search = Customer.search(params[:q])
    @customers = @search.result.page(params[:page])
    @offices_count = Office.group(:customer_id).count
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @customers }
    end
  end

  # GET /customers/1
  # GET /customers/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer }
    end
  end

  # GET /customers/new
  def new
    @customer = Customer.new
    @users = User.all
    @categories = Category.all
  end

  # GET /customers/1/edit
  def edit
    @users = User.all
    @categories = Category.all
  end

  # POST /customers
  # POST /customers.json
  def create
    @users = User.all
    @categories = Category.all
    @customer = Customer.new(customer_params)

    respond_to do |format|
      if @customer.save
        format.html { redirect_to @customer, notice: t(:customer_created) }
        format.json { render :show, status: :created, location: @customer }
      else
        format.html { render :new }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customers/1
  # PATCH/PUT /customers/1.json
  def update
    @users = User.all
    @categories = Category.all
    respond_to do |format|
      if @customer.update(customer_params)
        format.html { redirect_to @customer, notice: t(:customer_updated) }
        format.json { render :show, status: :ok, location: @customer }
      else
        format.html { render :edit }
        format.json { render json: @customer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customers/1
  # DELETE /customers/1.json
  def destroy
    @customer.destroy
    respond_to do |format|
      format.html { redirect_to customers_url, notice: t(:customer_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_params
      params.require(:customer).permit(:name, :desc, :user_id, :category_ids => [])
    end
end
