class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, only: [:show, :edit, :update, :destroy, :edit_password, :update_password]
  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    @roles = Role.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @roles = Role.all

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: t(:user_created) }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @roles = Role.all

    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to users_path, notice: t(:user_updated) }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /users/1/edit
  def edit
    @roles = Role.all
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_path, notice: t(:user_destroyed) }
      format.json { head :no_content }
    end
  end

  # GET /users
  # GET /users.json
  def index
    @roles = Role.all
    @search = User.search(params[:q])
    @users = @search.result.page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end
  # GET /users/1
  # GET /users/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit_password
  def edit_password
  end

  # PUT /users/1/edit_password
  def update_password
    respond_to do |format|
      if @user.update_attributes(user_params)
        format.html { redirect_to users_path, notice: t(:password_updated) }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "edit_password" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation, :language, :role_ids)
  end
end
