class AddressesController < ApplicationController
  before_action :set_customer
  before_action :set_office
  before_action :set_variables, only: [:new, :edit, :update, :create]
  before_action :set_address, only: [:show, :edit, :update, :destroy]

  # GET /addresses
  # GET /addresses.json
  def index
    @addresses = Address.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @addresses }
    end
  end

  # GET /addresses/1
  # GET /addresses/1.json
  def show
  end

  # GET /addresses/new
  def new
    @address = Address.new
  end

  # GET /addresses/1/edit
  def edit
    @states = @address.country.states
    @cities = @address.state.cities
    @streets = @address.city.streets
    @houses = @address.street.houses
  end

  # POST /addresses
  # POST /addresses.json
  def create
    unless params[:address][:country_id].blank?
      @states = Country.find(params[:address][:country_id]).states
    end
    unless params[:address][:state_id].blank?
      @cities = State.find(params[:address][:state_id]).cities
    end
    unless params[:address][:city_id].blank?
      @streets = City.find(params[:address][:city_id]).streets
    end
    unless params[:address][:street_id].blank?
      @houses = Street.find(params[:address][:street_id]).houses
    end
    @address = Address.new(address_params)
    @address.office = @office
    respond_to do |format|
      if @address.save
        format.html { redirect_to customer_office_path(@customer, @office), notice: t(:address_created) }
        format.json { render :show, status: :created, location: @address }
      else
        format.html { render :new }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /addresses/1
  # PATCH/PUT /addresses/1.json
  def update
    unless params[:address][:country_id].blank?
      @states = Country.find(params[:address][:country_id]).states
    end
    unless params[:address][:state_id].blank?
      @cities = State.find(params[:address][:state_id]).cities
    end
    unless params[:address][:city_id].blank?
      @streets = City.find(params[:address][:city_id]).streets
    end
    unless params[:address][:street_id].blank?
      @houses = Street.find(params[:address][:street_id]).houses
    end

    respond_to do |format|
      if @address.update(address_params)
        format.html { redirect_to customer_office_path(@customer, @office), notice: t(:address_updated) }
        format.json { render :show, status: :ok, location: @address }
      else
        format.html { render :edit }
        format.json { render json: @address.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /addresses/1
  # DELETE /addresses/1.json
  def destroy
    @address.destroy
    respond_to do |format|
      format.html { redirect_to customer_office_path(@customer, @office), notice: t(:address_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_office
      @office = Office.find(params[:office_id])
    end

    def set_variables
      @countries = Country.all
      # @states = State.all
      # @cities = City.all
      # @streets = Street.all
      # @houses = House.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:country_id, :state_id, :city_id, :street_id, :house_id)
    end
end
