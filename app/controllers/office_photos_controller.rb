class OfficePhotosController < ApplicationController
  load_and_authorize_resource
  before_action :set_office_photo, only: [:show, :edit, :update, :destroy]
  before_action :set_customer
  before_action :set_office

  # GET /office_photos
  # GET /office_photos.json
  def index
    @office_photos = @office.office_photos.all.page(params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @office_photos }
    end
  end

  # GET /office_photos/1
  # GET /office_photos/1.json
  def show
  end

  # GET /office_photos/new
  def new
    @office_photo = @office.office_photos.new
  end

  # GET /office_photos/1/edit
  def edit
  end

  # POST /office_photos
  # POST /office_photos.json
  def create
    @office_photo = @office.office_photos.new(office_photo_params)

    respond_to do |format|
      if @office_photo.save
        format.html { redirect_to customer_office_office_photos_path(@customer, @office), notice: t(:photo_created) }
        format.json { render :show, status: :created, location: @office_photo }
      else
        format.html { render :new }
        format.json { render json: @office_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /office_photos/1
  # PATCH/PUT /office_photos/1.json
  def update
    respond_to do |format|
      if @office_photo.update(office_photo_params)
        format.html { redirect_to customer_office_office_photos_path(@customer, @office), notice: t(:photo_updated) }
        format.json { render :show, status: :ok, location: @office_photo }
      else
        format.html { render :edit }
        format.json { render json: @office_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /office_photos/1
  # DELETE /office_photos/1.json
  def destroy
    @office_photo.destroy
    respond_to do |format|
      format.html { redirect_to customer_office_office_photos_path(@customer, @office), notice: t(:photo_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office_photo
      @office_photo = OfficePhoto.find(params[:id])
    end

    def set_customer
      @customer = Customer.find(params[:customer_id])
    end

    def set_office
      @office = @customer.offices.find(params[:office_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_photo_params
      params.fetch(:office_photo, {}).permit(:file)
    end
end
