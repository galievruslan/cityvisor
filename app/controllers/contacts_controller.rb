class ContactsController < ApplicationController
  load_and_authorize_resource
  # GET /customer/1/contacts
  # GET /customer/1/contacts.json
  def index
    @customer = Customer.find(params[:customer_id])
    @office = @customer.offices.find(params[:office_id])
    @search = @office.contacts.search(params[:q])
    @contacts = @search.result.page(params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contacts }
    end
  end
end
