class PrivateAssetsController < ApplicationController  
  
  def show
    if params[:file] && params[:asset_type]
      path = Rails.root.to_s + '/private/uploads/' + params[:asset_type] + '/' + params[:file] + '.' +  params[:format]      
      if File.file?(path)
        send_file path, type: 'image/png', disposition: 'inline', :x_sendfile => true
      else
        render nothing: true, status: :not_found
      end
    else
      render nothing: true, status: :not_found
    end
  end
end
