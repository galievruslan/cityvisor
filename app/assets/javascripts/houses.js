$(document).on('page:change', function() {
  $("#house_latitude").TouchSpin({min: 0, step: 0.000001, decimals: 6, boostat: 1, maxboostedstep: 1000});
  $("#house_longitude").TouchSpin({min: 0, step: 0.000001, decimals: 6, boostat: 1, maxboostedstep: 1000});
});
