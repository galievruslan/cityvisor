$(document).ready(function() {
  $("#address_country_id").select2({allowClear: true});
  $("#address_state_id").select2({allowClear: true});
  $("#address_city_id").select2({allowClear: true});
  $("#address_street_id").select2({allowClear: true});
  $("#address_house_id").select2({allowClear: true});
});

function onCountryChanged(country_id) {
  jQuery("#states").html('');
  jQuery("#cities").html('');
  jQuery("#streets").html('');
  jQuery("#houses").html('');
  jQuery.ajax({
    url: "/countries/" + country_id + "/states",
    type: "GET",
    dataType: "html",
    success: function(data) {
      jQuery("#states").html(data);
      jQuery("#address_state_id").select2({allowClear: true});
    }
  });
}

function onStateChanged(state_id) {
  jQuery("#cities").html('');
  jQuery("#streets").html('');
  jQuery("#houses").html('');
  jQuery.ajax({
    url: "/states/" + state_id + "/cities",
    type: "GET",
    dataType: "html",
    success: function(data) {
      jQuery("#cities").html(data);
      jQuery("#address_city_id").select2({allowClear: true});
    }
  });
}

function onCityChanged(city_id) {
  jQuery("#streets").html('');
  jQuery("#houses").html('');
  jQuery.ajax({
    url: "/cities/" + city_id + "/get_streets",
    type: "GET",
    dataType: "html",
    success: function(data) {
      jQuery("#streets").html(data);
      jQuery("#address_street_id").select2({allowClear: true});
    }
  });
}

function onStreetChanged(street_id) {
  jQuery("#houses").html('');
  city_id = jQuery('#address_city_id option:selected').val();
  jQuery.ajax({
    url: "/cities/" + city_id + "/streets/" + street_id + "/get_houses",
    type: "GET",
    dataType: "html",
    success: function(data) {
      jQuery("#houses").html(data);
      jQuery("#address_house_id").select2({allowClear: true});
    }
  });
}