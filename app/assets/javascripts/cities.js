$(document).ready(function() {
  $("#city_state_id").select2({allowClear: true});
  $("#country_id").select2({allowClear: true});
});
function update_states(country_id) {
  jQuery.ajax({
    url: "/cities/states_in_country",
    type: "GET",
    data: {"country_id" : country_id},
    dataType: "html",
    success: function(data) {
      jQuery("#states").html(data);
      $("#city_state_id").select2({allowClear: true});
    }
  });
};