// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
// REMOVED TURBOLINKS
//= require jquery

//= require jquery_ujs
//= require bootstrap
//= require select2
//= require select2_locale_ru
//= require bootstrap-inputmask.min
//= require bootstrap-fileupload

//= require blueimp-gallery.min
//= require bootstrap-touchspin
//= require blueimp-gallery.min
/* ===================================================
 Reset button for filter form
* ===================================================*/
!function($) {
  $(window).on('load', function(){
    $('#q_reset').click(function(){
      $('.search-field').val('');
      $("select.search-field").each(function() {
        $(this).select2("val", "");
      });
    });
  });
}(window.jQuery);

/* ===================================================
 Add icon to buttons
* ===================================================*/
$(function() {
  $('.btn-new').prepend('<i class="fa fa-file"></i> ');
  $('.btn-edit').prepend('<i class="fa fa-edit"></i> ');
  $('.btn-remove').prepend('<i class="fa fa-remove"></i> ');
  $('.btn-back').prepend('<i class="fa fa-hand-o-left"></i> ');
});
